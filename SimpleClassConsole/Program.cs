﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleClassLibrary;

namespace SimpleClassConsole
{
    class Program
    {
        static void ReadStudentsArray(int n)
        {
            Console.WriteLine();
            Student[] student = new Student[n];
            Student students = new Student();

            for (int i = 0; i < n; i++)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Введіть ім'я студента: ");
                string name = Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.Write("Введіть прізвище студента: ");
                string surname = Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("Введіть шифр групи: ");
                string group = Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("Введіть номер групи: ");
                string year = Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("Введіть назву предмета: ");
                string subject = Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.Write("Введіть прізвище та ініціали викладача: ");
                string teacher = Console.ReadLine();

                int points;
                do
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("Введіть оцінку за даний предмет у 100-бальній шкалі: ");
                    if (!int.TryParse(Console.ReadLine(), out points))
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("Error!");
                    }
                    else
                        break;
                } while (true);

                
               
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                    Console.WriteLine("Виберіть термін оплатити за навчання:");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(" 1. за місяць");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine(" 2. за рік (10 місяців)");
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine(" 3. за весь період навчання (40 місяців)");

                    int k;
                    do
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.Write("Вибрати: ");
                        if (!int.TryParse(Console.ReadLine(), out k))
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("Помилка! Введіть значення із діапазону 1..3");
                        }
                        else
                            break;
                    } while (true);

                    switch (k)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        default:
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("Помилка! Введіть значення із діапазону 1..3");
                            break;
                    }
                 
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    
                    double costOfEducation;
                do
                { 
                    Console.Write("Введіть ціну в гривнях за навчання: ");
                    if (!double.TryParse(Console.ReadLine(), out costOfEducation))
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.WriteLine("Помилка!");
                    }

                    else
                        break;
                } while (true);

                    double oneMonth = 0;
                    double tenMonth = 0;
                    double fortyMonth = 0;
                if (k == 1)    //Грн і місяць
                {
                    oneMonth = costOfEducation;
                    tenMonth = 10 * costOfEducation;
                    fortyMonth = 40 * costOfEducation;
                }
                if (k == 2)
                {
                    oneMonth = costOfEducation / 10;
                    tenMonth = costOfEducation;
                    fortyMonth = costOfEducation * 4;
                }
                if (k == 3)   //Грн і 40 місяців
                {
                    oneMonth = costOfEducation / 40;
                    tenMonth = costOfEducation / 4;
                    fortyMonth = costOfEducation;

                }
                Console.WriteLine();
                Console.WriteLine("За місяць - " + oneMonth);
                Console.WriteLine("За рік - " + tenMonth);
                Console.WriteLine("За весь період навчання - " + fortyMonth);
                student[i] = new Student(subject, teacher, points, name, surname, group, year, oneMonth);
                Console.WriteLine();

            }
            Console.WriteLine();
  
            Menu(student, n, students);
        }

        static void PrintStudent(Student[] student, int n)
        {
            Student students = new Student();

            for (int i = 0; i < n; i++)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Предмет: " + student[i].Subject + ", " + student[i].Teacher);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Cтудент : " + student[i].Surname + " " + student[i].Name);
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Група: " + student[i].Group + "-" + student[i].Year);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Оцінка: " + student[i].Points);
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Вартість навчання за місяць: " + student[i].PaymentForStudying);
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Вартість навчання за рік: " + student[i].PaymentForStudying * 10);
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.WriteLine("Вартість навчання за весь період: " + student[i].PaymentForStudying * 40);
                Console.WriteLine();
            }

            Console.WriteLine();
            Menu(student, n, students);
        }

        static void PrintStudents(Student[] student, int n)
        {
            Student students = new Student();

            for (int i = 0; i < n; i++)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Предмет: " + student[i].Subject + ", " + student[i].Teacher);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Cтудент : " + student[i].Surname + " " + student[i].Name);
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Група: " + student[i].Group + "-" + student[i].Year);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Оцінка: " + student[i].Points);
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Вартість навчання за місяць: " + student[i].PaymentForStudying);
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Вартість навчання за рік: " + student[i].PaymentForStudying * 10);
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.WriteLine("Вартість навчання за весь період: " + student[i].PaymentForStudying * 40);
                Console.WriteLine();
            }
            Console.WriteLine();
            Menu(student, n, students);
        }

        static void GetStudentsInfo(Student[] student, int n)
        {
            int max = student[0].Points;
            int min = student[0].Points;
            int _max = 0;
            int _min = 0;

            for (int i = 0; i < student.Length; i++)
            {
                if (max <= student[i].Points)
                {
                    _max = student[i].Points;
                }
            }

            for (int i = 0; i < student.Length; i++)
            {
                    if (min >= student[i].Points)
                    {
                        _min = student[i].Points;
                    }
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Предмет, за який студент отримав найкращий бал - " + _max);
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Предмет, за який студент отримав найгірший бал - " + _min);
        }

        static void SortStudentsByPoints(Student[] student, int n)  //Сортування за середнім балом студента
        {
            Array.Sort(student);
            for (int i = 0; i < student.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Предмет: " + student[i].Subject + ", " + student[i].Teacher);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Cтудент : " + student[i].Surname + " " + student[i].Name);
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.WriteLine("Група: " + student[i].Group + "-" + student[i].Year);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Оцінка: " + student[i].Points);
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Вартість навчання за місяць: " + student[i].PaymentForStudying);
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Вартість навчання за рік: " + student[i].PaymentForStudying * 10);
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.WriteLine("Вартість навчання за весь період: " + student[i].PaymentForStudying * 40);
                Console.WriteLine();
            }
        }

        static void SortStudentsByName(Student[] student, int n)    //Сортування за прізвищем, якщо прізвища
        {                                                        // однакові, то розташувати об'єкти за ім'ям
            List<string> firstName = new List<string>();

            for (int i = 0; i < student.Length; i++)
            {

                firstName.Add(student[i].Name);
            }
            firstName.Sort();
           
            for (int i = 0; i < student.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Предмет: " + student[i].Subject + ", " + student[i].Teacher);
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
                Console.WriteLine("Cтудент : " + student[i].Surname + " " + student[i].Name);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Група: " + student[i].Group + "-" + student[i].Year);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Оцінка: " + student[i].Points);
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Вартість навчання за місяць: " + student[i].PaymentForStudying);
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Вартість навчання за рік: " + student[i].PaymentForStudying * 10);
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.WriteLine("Вартість навчання за весь період: " + student[i].PaymentForStudying * 40);
                Console.WriteLine();
            }
        }

        static void Menu(Student[] st, int n, Student student)
        {
            /*Меню*/
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Меню:");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(" 1. Записати дані");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(" 2. Показати дані");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(" 3. Обрахувати середнє арифметичне усіх оцінок");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(" 4. Показати назву предмета із найвищим балом");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(" 5. Показати назву предмета із найгіршим балом");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(" 6. Сортування за середнім балом");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(" 7. Сортування за прізвищем");
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine(" 8. Показати найбільший та найменший бали");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine(" 9. Вийти");
            Console.WriteLine();

            int k;
            do
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.Write("Виберіть дію: ");
                if (!int.TryParse(Console.ReadLine(), out k))
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Помилка! Введіть ціле число з діапазону 1...8");
                }   
                else
                    break;
            } while (true);

            Student[] _student = st;
            Student students = new Student();

            switch (k)
            {
                case 1:
                    ReadStudentsArray(n);
                    break;
                case 2:
                    if (n == 1)
                    {
                        Console.WriteLine();
                        PrintStudent(st, n);
                    }
                    else
                    {
                        Console.WriteLine();
                        PrintStudents(st, n);
                    }
                    break;
                case 3:
                    student.GetAveragePoints(st, n);
                    Menu(_student, n, students);
                    break;
                case 4:
                    student.GetBestSubject(st, n);
                    Menu(_student, n, students);
                    break;
                case 5:
                    student.GetWorstSubject(st, n);
                    Menu(_student, n, students);
                    break;
                case 6:
                    SortStudentsByPoints(st, n);
                    Menu(_student, n, students);
                    break;
                case 7:
                    SortStudentsByName(st, n);
                    Menu(_student, n, students);
                    break;
                case 8:
                    GetStudentsInfo(st, n);
                    Menu(_student, n, students);
                    break;
                case 9:
                    Environment.Exit(0);
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Помилка введення!");
                    break;
            }
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Лабораторна робота №7.\nВиконав: Ніколайчук Р.О., група СН-21\nВаріант №9\n");

            int n;
            do
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.Write("Введіть кількість предметів: ");

                if (!int.TryParse(Console.ReadLine(), out n))
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Помилка! Введіть ціле число");
                }
                else
                    break;
            } while (true);

            Student[] student = new Student[n];
            Student students = new Student();

            Menu(student, n, students);
            Console.ReadKey();
        }
    }
}