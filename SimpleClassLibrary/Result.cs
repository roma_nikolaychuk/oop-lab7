﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{
    public class Result : IComparable      //Представляє результатии сесії з одного предмета
    {
        protected string subject;   //Назва предмет

        public string Subject
        {
            get
            {
                return subject;
            }
            set
            {
                subject = value;
            }
        }

        protected string teacher;    //ПІБ викладача

        public string Teacher
        {
            get
            {
                return teacher;
            }
            set
            {
                teacher = value;
            }
        }

        protected int points;         //Оцінка за 100-бальною системою

        public int Points
        {
            get
            {
                return points;
            }
            set
            {
                points = value;
            }
        }

        public Result(string subject, string teacher, int points)
        {
            Subject = subject;
            Teacher = teacher;
            Points = points;
        }

        public Result(Result result)    //Конструктор копіювання
        {
            this.Subject = result.Subject;
            this.Teacher = result.Teacher;
            this.Points = result.Points;
        }

        public Result()
        {

        }
        public interface IComparable
        {
            int CompareTo(object obj);
        }


        public int CompareTo(object obj)
        {
            Student p = obj as Student;
            if (p != null)
            {
                if (this.Points < p.Points)
                    return -1;
                else if (this.Points > p.Points)
                    return 1;
                else
                    return 0;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                throw new Exception("Неможливо порівняти два об'єкта");
            }
        }
    }
}
