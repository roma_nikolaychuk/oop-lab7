﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{
   public class Student : Result
    {

        protected string name;           //Ім'я

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        protected string surname;           //Прізвище 

        public string Surname
        {
            get
            {
                return surname;
            }
            set
            {
                surname = value;
            }
        }

        protected string group;            //Шифр групи

        public string Group
        {
            get
            {
                return group;
            }
            set
            {
                group = value;
            }
        }

        protected string year;            //Номер курсу

        public string Year
        {
            get
            {
                return year;

            }
            set
            {
                year = value;
            }
        }

        protected double paymentForStudying;

        public double PaymentForStudying
        {
            get
            {
                return paymentForStudying;
            }
            set
            {
                paymentForStudying = value;
            }
        }

        public Student(string subject, string teacher, int points, string name, string surname, string group, string year, double costOfEducation) : base(subject, teacher, points)
        {
            this.subject = subject;
            this.teacher = teacher;
            this.points = points;
            this.name = name;
            this.surname = surname;
            this.group = group;
            this.year = year;
            this.paymentForStudying = costOfEducation;

        }

        public Student(Student student)   //Конструктор копіювання
        {
            this.Subject = student.Subject;
            this.Teacher = student.Teacher;
            this.Points = student.Points;
            this.Name = student.Name;
            this.Surname = student.Surname;
            this.Group = student.Group;
            this.Year = student.Year;
            this.PaymentForStudying = student.PaymentForStudying;
        }

        public Student()
        {

        }

        public void GetAveragePoints(Student[] student, int n)  //Обраховує середнє 
        {                                                       //арифметичне усіх оцінок
            double Suma = 0;

            for (int i = 0; i < n; i++)
            {
                Suma += student[i].Points;
            }
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Середнє арифметичне - {0:0.##}", Suma / n);
        }

        public void GetBestSubject(Student[] student, int n)   //Повертає назву предмета, за яким студент
        {                                                     //має найвищий бал серед інших предметів
            int max = student[0].Points;
            int min = student[0].Points;

            string _max = "";
            string _min = "";

            for (int i = 0; i < student.Length; i++)
            {
                if (max <= student[i].Points)
                {
                    _max = student[i].Subject;
                }
            }

            for (int i = 0; i < student.Length; i++)
            {
                if (min >= student[i].Points)
                {
                    _min = student[i].Subject;
                }
            }
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Предмет, за який студент отримав найкращий бал - " + _max);
            Console.WriteLine();
        }

        public void GetWorstSubject(Student[] student, int n) //Повертає назву предмета, за яким студент 
        {                                                    //отримав найгірший бал
            int max = student[0].Points;
            int min = student[0].Points;

            string _max = "";
            string _min = "";

            for (int i = 0; i < student.Length; i++)
            {
                if (max <= student[i].Points)
                {
                    _max = student[i].Subject;
                }
            }

            for (int i = 0; i < student.Length; i++)
            {
                if (min >= student[i].Points)
                {
                    _min = student[i].Subject;
                }
            }
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Предмет, за який студент отримав найгірший бал - " + _min);
        }
    }
}
